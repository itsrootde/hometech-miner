const DB = require('./database-operations');
const argon2 = require('argon2');

// Accepts the connection if the username and password are valid
var authenticate = function(client, username, password, callback) {
    DB.User.findOne({ username: username }, function(err, doc) {
        if(user){
            argon2.verify(doc.devicelogintoken, password).then(match => {
                if (match) {
                  var date = new Date();
                  DB.User.findOneAndUpdate({username:username},{last_login : date.getTime()}, (err, doc) =>{
                    if(err){
                        console.log('ERROR in databaes execution ' + err);
                    }
                  });
                  callback(null, true);
                 } else {
                console.log('Credentials WRONG');
                  callback(null, false);
                }
              }).catch(err => {
                console.log('Credentials WRONG');
                console.log(err);
                callback(null, false);
              });
        }else{
            console.log('Credentials WRONG');
            callback(null, false);
        }
        
    });
  }
  
  // In this case the client authorized as device can publish to /device/deviceid taking
  // the username from the topic and verifing it is the same of the authorized user
  var authorizePublish = function(client, topic, payload, callback) {
    if(topic.split('/')[0] == 'device'){
        if(client.user == topic.split('/')[1]){
            callback(null, true);
        }else{callback(null, false)}
    }else{callback(null, false)}
  }
  
  // In this case the client authorized as device can subscribe to /control/deviceid taking
  // the username from the topic and verifing it is the same of the authorized user
  var authorizeSubscribe = function(client, topic, callback) {
    callback(null, false); //currently disabled
  
  }

  module.exports = {
      authenticate = authenticate,
      authorizePublish = authorizePublish,
      authorizeSubscribe = authorizeSubscribe
  }