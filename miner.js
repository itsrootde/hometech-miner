const mosca = require('mosca');
const auth = require('./authenticate');

var moscaSettings = {
    port: 1883,
    persistence: {
        factory: mosca.persistence.Memory
  }
};

var server = new mosca.Server(moscaSettings);
server.on('ready', setup);

server.on('clientConnected', function(client) {
        console.log('client connected', client.id);     
});

// fired when a message is received
server.on('published', function(packet, client) {
    console.log('Published', packet.payload);
});

// fired when the mqtt server is ready
function setup() {
    server.authenticate = auth.authenticate;
    server.authorizePublish = auth.authorizePublish;
    server.authorizeSubscribe = auth.authorizeSubscribe;
    console.log('Mosca server is up and running ...');
}