const mongoose = require('mongoose');
const Influx = require('influx');
var Schema = mongoose.Schema;
 
mongoose.connect('mongodb://localhost:27017/hometech', {useNewUrlParser : true});
// Device Schema
var deviceSchema = new Schema({
  userid: String,
  devicename: String,
  devicetype: String,
  devicelogintoken: String,
  created_at: Date,
  last_login: Date
});

var Device = mongoose.model('Device', deviceSchema, 'device');


const influxtemp = new Influx.InfluxDB({
  host: 'localhost',
  database: 'hometech',
  schema: [{
    measurements: 'temperature',
    fields: {
      temperature: Influx.FieldType.FLOAT
    },
    tags: [
      'deviceid'
    ]
  }]
})

module.exports = {
  Device: Device
}